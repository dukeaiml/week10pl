# Rust Serverless Transformer Endpoint for Translation


## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation and Usage](#Installation and Usage)
4. [Development Process](#development-process)

## Deliverables
Dockerfile and Rust code included in repo.

cURL request:
![Testing report](Screenshot_2024-04-14_at_7.56.54_PM.png)

Lambda Screenshot:
![Screenshot](Screenshot_2024-04-14_at_8.26.34_PM.png)


## Description


This is a Rust AWS Lambda function integrated with rust-bert which allows the user to use REST API to input a prompt and language selction in a JSON, which calls an M2M100 model. This returns the translation of the prompt in the language. 

It is complete with a Dockerfile for containerization and an implemented query endpoint.

Check src/main.rs to see the source code.

## Installation and Usage

To run this project locally, ensure Rust, Cargo, are installed on your system. Follow these installation steps:

1. Clone the repository:

git clone https://gitlab.com/dukeaiml/week10pl.git

2. Invoke the function with

cargo run 

or:

docker build -t translation-serv .
and docker run -p 3030:3030 translation-serv

or cURL with my function cURL
https://ao3jbfol3fsubd6nbgaodnu4za0ctneq.lambda-url.us-east-1.on.aws/



## Development Process

Development steps included:

1. Environment Setup: Install the necessary dependencies for rust.
2. Implementation: Write translation_handler for handling the server.
3. Implementation: Write translation_text to incorporate rust_bert and language functionality.
3. Testing: Ensuring functionality and handling errors. Use cURL requests.
4. Documentation: Documenting code and writing this README.


