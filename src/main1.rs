use rust_bert::pipelines::translation::{Language, TranslationModelBuilder};
use rust_bert::pipelines::common::ModelType;
use serde::{Deserialize, Serialize};
use warp::{Filter, Rejection, Reply, http::StatusCode, reply::json};
use warp::reply;

#[derive(Deserialize)]
struct TranslationRequest {
    text: String,
    target_language: String,
}

#[derive(Serialize)]
struct TranslationResponse {
    translated_text: String,
}

async fn translate_handler(body: TranslationRequest) -> Result<impl Reply, Rejection> {
    match translate_text(&body.text, &body.target_language).await {
        Ok(translated_text) => {
            // Successfully translated text
            Ok(json(&TranslationResponse { translated_text }))
        },
        Err(e) => {
            // Error handling, returning internal server error
            eprintln!("Error during translation: {}", e);
            let error_message = serde_json::json!({"error": "Internal server error"});
            Ok(reply::json(&error_message))
        }
    }
}

async fn translate_text(input: &str, target: &str) -> Result<String, anyhow::Error> {
    let model = TranslationModelBuilder::new()
        .with_model_type(ModelType::Marian)
        .with_source_languages(vec![Language::English])
        .with_target_languages(vec![Language::French, Language::Spanish])
        .create_model()?;

    let target_language = match target {
        "French" => Language::French,
        "Spanish" => Language::Spanish,
        _ => anyhow::bail!("Unsupported language"),
    };

    let output = model.translate(&[input], None, target_language)?;
    Ok(output.join(" "))
}

#[tokio::main]
async fn main() {
    let translate_route = warp::post()
        .and(warp::path("translate"))
        .and(warp::body::json())
        .and_then(translate_handler);

    warp::serve(translate_route)
        .run(([0, 0, 0, 0], 3030))
        .await;
}
